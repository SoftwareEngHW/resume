
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");

//An image to overlay
$overlay.append($image);

//Add overlay
$("body").append($overlay);

//click the image and a scaled version of the full size image will appear
$("#photo-gallery a").click( function(event) {
    event.preventDefault();
    var imageLocation = $(this).attr("href");

    //update overlay with the image linked in the link
    $image.attr("src", imageLocation);

    //show the overlay
    $overlay.show();
} );

$("#overlay").click(function() {
    $( "#overlay" ).hide();
});

// var galleryFeed = new Instafeed({
//     get: "user",
//     userId: 4622774,
//     accessToken: "4622774.7cbaeb5.ec8c5041b92b44ada03e4a4a9153bc54",
//     resolution: "standard_resolution",
//     useHttp: "true",
//     limit: 6,
//     template: '                 <a data-pinterest-text="Pin it" data-tweet-text="share it on instagram" class="" href="{{image}}" data-sub-html="{{image}}">\n' +
//         '                            <img class="img-responsive" src="{{image}}">\n' +
//         '                            <div class="demo-gallery-poster">\n' +
//         '                                <img src="{{image}}">\n' +
//         '                            </div>\n' +
//         '                            \n' +
//         '                        </a>',
//     // template: '<div class="col-xs-12 col-sm-6 col-md-4"><a href="{{image}}"><div class="img-featured-container"><div class="img-backdrop"></div><div class="description-container"><p class="caption">{{caption}}</p><span class="likes"><i class="icon ion-heart"></i> {{likes}}</span><span class="comments"><i class="icon ion-chatbubble"></i> {{comments}}</span></div><img src="{{image}}" class="img-responsive"></div></a></div>',
//     target: "aniimated-thumbnials",
//     after: function() {
//         // disable button if no more results to load
//         if (!this.hasNext()) {
//             btnInstafeedLoad.setAttribute('disabled', 'disabled');
//         }
//     },
// });
// galleryFeed.run();
//
// var btnInstafeedLoad = document.getElementById("btn-instafeed-load");
// btnInstafeedLoad.addEventListener("click", function() {
//     galleryFeed.next()
// });
