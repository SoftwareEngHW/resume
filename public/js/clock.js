window.onload = function () {
    startTime();
};

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    m = checkTime(m);
    h = checkTime(h);
    document.getElementById("time").innerText = h + ":" + m;
    document.getElementById("date").innerText = today.toUTCString();
    setTimeout(startTime, 1000);
    console.log(today, h + ":" + m)
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i
    }  // add zero in front of numbers < 10
    return i;
}